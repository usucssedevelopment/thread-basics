﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Threads
{
    public class Consumer
    {
        public ConcurrentBag<Widget> WidgetBag { get; set; }

        private bool _running;
        private Task _myTask;
        private readonly Random _random = new Random();


        public void Start()
        {
            if (_running) return;

            _running = true;
            _myTask = new Task(Run);
            _myTask.Start();
            Console.WriteLine("Consumer Started");
        }

        public void Stop()
        {
            if (!_running) return;

            _running = false;                       // Terminate the run loop, allowing the thread stop gracefully
            if (_myTask?.Wait(1000) == false)      // Wait for the thread to actually stop
                throw new ApplicationException("Cannot terminate Producer's thread");
            _myTask = null;
            Console.WriteLine("Consumer Stopped");
        }

        private void Run()
        {
            while (_running)
            {
                Thread.Sleep(_random.Next(100,500));  // Simulate some other work being done
                Widget widget;
                if (WidgetBag.TryTake(out widget))
                    Console.WriteLine($"Took {widget} from bag");
            }
        }
    }
}
