﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Threads
{
    public class Producer
    {
        public ConcurrentBag<Widget> WidgetBag { get; set; }

        private bool _running;
        private Task _myTask;
        private int _nextWidgetId = 1;
        private readonly Random _random = new Random();

        public void Start()
        {
            if (_running) return;

            _running = true;
            _myTask = new Task(Run);
            _myTask.Start();
            Console.WriteLine("Producer Started");
        }

        public void Stop()
        {
            if (!_running) return;

            _running = false;                       // Terminate the run loop, allowing the thread stop gracefully
            if (_myTask?.Wait(1000)==false)        // Wait for the thread to actually stop
                throw new ApplicationException("Cannot terminate Producer's thread");
            _myTask = null;
            Console.WriteLine("Producer Stopped");
        }

        private void Run()
        {
            while (_running)
            {
                Thread.Sleep(_random.Next(100,500));  // Simulate some other work being done
                var widget = new Widget() {Id = _nextWidgetId++, Weight = _random.Next(100, 199)};
                Console.WriteLine($"Adding {widget} to the widget bag");
                WidgetBag.Add(widget);            
            }
        }
    }
}
