# Thread-basics

This repository contains simple examples of using threads in CSharp and Java. Each example has three
classes: Producer, Consumer, and Widget.  A Producer object creates Widget objects and puts them into
a shared bag.  A Consumer object takes Widget objects out of the bag.  The main program creates a
thread-safe bag, a producer, and a consumer object.  It then starts both the producer and consumer
and lets them run for 20 seconds.  After that, it stops them gracefully.


