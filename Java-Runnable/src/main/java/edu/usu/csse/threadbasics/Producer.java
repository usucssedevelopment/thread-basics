package edu.usu.csse.threadbasics;

import java.util.concurrent.LinkedBlockingQueue;

class Producer implements Runnable
{
	private LinkedBlockingQueue<Widget> widgets;

	private boolean _running;
	private Thread _myThread;
	private int _nextWidgetId = 1;
	private final java.util.Random _random = new java.util.Random();

	Producer(LinkedBlockingQueue<Widget> widgets) {
		this.widgets = widgets;
	}

	final void start()
	{
		if (_running)
		{
			return;
		}

		_running = true;
		_myThread = new Thread(this);
		_myThread.start();
		System.out.println("Producer Started");
	}

	void end() throws Exception
	{
		if (!_running)
		{
			return;
		}

		_running = false; 								// Terminate the run loop, allowing the thread stop gracefully
		int remainingTime = 1000;
		while (_myThread.isAlive() && remainingTime>0)  // Wait for the thread to actually stop
		{
			Thread.sleep(10);
			remainingTime -= 10;
		}

		if (_myThread.isAlive())
		{
			throw new Exception("Cannot terminate Producer's thread");
		}
		_myThread = null;
		System.out.println("Producer Stopped");
	}

	public void run() 
	{
		while (_running)
		{

			try {
				Thread.sleep(_random.nextInt(100)+400);	// Simulate some other work being done

				Widget widget = new Widget(_nextWidgetId++, _random.nextInt(100)+99);

				System.out.println("Adding "+ widget.toString() +" to the widget bag");
				widgets.put(widget);

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}