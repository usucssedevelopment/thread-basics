package edu.usu.csse.threadbasics;

import java.util.concurrent.LinkedBlockingQueue;

public class Program
{
	public static void main(String[] args) throws Exception
	{
		LinkedBlockingQueue<Widget> widgetBag = new LinkedBlockingQueue<>();

		Producer producer = new Producer(widgetBag);
		Consumer consumer = new Consumer(widgetBag);

		producer.start();
		consumer.start();

		System.out.println("Type any key and ENTER to exit");
		System.in.read();
		System.exit(0);

		producer.end();
		consumer.end();
	}
}