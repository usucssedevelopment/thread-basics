package edu.usu.csse.threadbasics;

import java.util.NoSuchElementException;
import java.util.concurrent.LinkedBlockingQueue;

class Consumer implements Runnable
{
	private LinkedBlockingQueue<Widget> widgets;

	private boolean _running;
	private Thread _myThread;
	private final java.util.Random _random = new java.util.Random();

	Consumer(LinkedBlockingQueue<Widget> widgets) {
		this.widgets = widgets;
	}

	void start()
	{
		if (_running)
		{
			return;
		}

		_running = true;
		_myThread = new Thread(this);
		_myThread.start();
		System.out.println("Consumer Started");
	}

	void end() throws Exception
	{
		if (!_running)
		{
			return;
		}

		_running = false; 								// Terminate the run loop, allowing the thread stop gracefully
		int remainingTime = 1000;
		while (_myThread.isAlive() && remainingTime>0)  // Wait for the thread to actually stop
		{
			Thread.sleep(10);
			remainingTime -= 10;
		}

		if (_myThread.isAlive()) // Wait for the thread to actually stop
		{
			throw new Exception("Cannot terminate Consumer's thread");
		}
		_myThread = null;
		System.out.println("Consumer Stopped");
	}

	public void run()
	{
		while (_running)
		{
			try {
				Thread.sleep(_random.nextInt(100) + 400);         // Simulate some other work being done
				Widget widget = widgets.remove();
				System.out.println("Took" + widget.toString() + " from bag");
			} catch (NoSuchElementException e) {
				// ignore
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
