﻿namespace Threads
{
    public class Widget
    {
        public int Id { get; set; }
        public int Weight { get; set; }

        public override string ToString()
        {
            return $"Widget {Id}, weight={Weight}";
        }
    }
}
