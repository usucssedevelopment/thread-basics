﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Threads
{
    public class Producer
    {
        public ConcurrentBag<Widget> WidgetBag { get; set; }

        private bool _running;
        private Thread _myThread;
        private int _nextWidgetId = 1;
        private readonly Random _random = new Random();

        public void Start()
        {
            if (_running) return;

            _running = true;
            _myThread = new Thread(Run);
            _myThread.Start();
            Console.WriteLine("Producer Started");
        }

        public void Stop()
        {
            if (!_running) return;

            _running = false;                       // Terminate the run loop, allowing the thread stop gracefully
            if (_myThread?.Join(1000)==false)        // Wait for the thread to actually stop
                throw new ApplicationException("Cannot terminate Producer's thread");
            _myThread = null;
            Console.WriteLine("Producer Stopped");
        }

        private void Run()
        {
            while (_running)
            {
                Thread.Sleep(_random.Next(100,500));  // Simulate some other work being done
                var widget = new Widget() {Id = _nextWidgetId++, Weight = _random.Next(100, 199)};
                Console.WriteLine($"Adding {widget} to the widget bag");
                WidgetBag.Add(widget);            
            }
        }
    }
}
