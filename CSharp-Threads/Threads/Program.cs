﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Threads
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var widgetBag = new ConcurrentBag<Widget>();
            var producer = new Producer() {WidgetBag = widgetBag};
            var consumer = new Consumer() {WidgetBag = widgetBag};

            producer.Start();
            consumer.Start();

            Console.WriteLine("Type any key to exit");
            Console.ReadKey();

            producer.Stop();
            consumer.Stop();
        }
    }
}
