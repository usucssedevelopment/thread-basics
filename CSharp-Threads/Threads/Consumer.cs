﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Threads
{
    public class Consumer
    {
        public ConcurrentBag<Widget> WidgetBag { get; set; }

        private bool _running;
        private Thread _myThread;
        private readonly Random _random = new Random();


        public void Start()
        {
            if (_running) return;

            _running = true;
            _myThread = new Thread(Run);
            _myThread.Start();
            Console.WriteLine("Consumer Started");
        }

        public void Stop()
        {
            if (!_running) return;

            _running = false;                       // Terminate the run loop, allowing the thread stop gracefully
            if (_myThread?.Join(1000) == false)      // Wait for the thread to actually stop
                throw new ApplicationException("Cannot terminate Producer's thread");
            _myThread = null;
            Console.WriteLine("Consumer Stopped");
        }

        private void Run()
        {
            while (_running)
            {
                Thread.Sleep(_random.Next(100,500));  // Simulate some other work being done
                Widget widget;
                if (WidgetBag.TryTake(out widget))
                    Console.WriteLine($"Took {widget} from bag");
            }
        }
    }
}
