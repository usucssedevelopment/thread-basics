package edu.usu.csse.threadbasics;

class Widget
{
	private int id;
	private int weight;

	Widget(int id, int weight) {
		this.id = id;
		this.weight = weight;
	}
	@Override
	public String toString()
	{
		return String.format("Widget %d, weight=%d", id, weight);
	}
}