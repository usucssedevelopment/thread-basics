package edu.usu.csse.threadbasics;

import java.util.NoSuchElementException;
import java.util.concurrent.LinkedBlockingQueue;

class Consumer extends Thread
{
	private LinkedBlockingQueue<Widget> widgets;

	private boolean _running;
	private final java.util.Random _random = new java.util.Random();

	Consumer(LinkedBlockingQueue<Widget> widgets) {
		this.widgets = widgets;
	}

	@Override
	public void start()
	{
		if (_running)
		{
			return;
		}

		_running = true;
		super.start();
		System.out.println("Consumer Started");
	}

	void end() throws Exception
	{
		if (!_running) {
			return;
		}

		_running = false; 							// Terminate the run loop, allowing the thread stop gracefully
		int remainingTime = 1000;
		while (isAlive() && remainingTime>0)  	  	// Wait for the thread to actually stop
		{
			Thread.sleep(10);
			remainingTime -= 10;
		}

		if (isAlive()) {
			throw new Exception("Cannot terminate Consumer's thread");
		}
		System.out.println("Consumer Stopped");
	}

	@Override
	public void run()
	{
		while (_running)
		{
			try {
				Thread.sleep(_random.nextInt(100) + 400);         // Simulate some other work being done
				Widget widget = widgets.remove();
				System.out.println("Took" + widget.toString() + " from bag");
			} catch (NoSuchElementException e) {
				// ignore
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
