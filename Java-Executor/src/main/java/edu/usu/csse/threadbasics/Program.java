package edu.usu.csse.threadbasics;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class Program
{
	public static void main(String[] args) throws Exception
	{
		ExecutorService executor = Executors.newFixedThreadPool(2);
		LinkedBlockingQueue<Widget> widgetBag = new LinkedBlockingQueue<>();

		Producer producer = new Producer(widgetBag);
		Consumer consumer = new Consumer(widgetBag);

		producer.start(executor);
		consumer.start(executor);

		System.out.println("Type any key and ENTER to exit");
		System.in.read();
		System.exit(0);

		producer.end();
		consumer.end();

	}
}