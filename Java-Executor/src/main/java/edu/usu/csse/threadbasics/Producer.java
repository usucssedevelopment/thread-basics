package edu.usu.csse.threadbasics;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

class Producer
{
	private static int nextWidgetId = 1;
	private LinkedBlockingQueue<Widget> widgets;
	private boolean _running;
	private final java.util.Random _random = new java.util.Random();
	private Future<?> taskFuture;

	Producer(LinkedBlockingQueue<Widget> widgets) {
		this.widgets = widgets;
	}

	void start(ExecutorService executor) { taskFuture = executor.submit(this::run);
	}

	void end() throws Exception {
		_running = false;
		int timeRemaining = 1000;
		while (!taskFuture.isDone() && timeRemaining > 0) {
			Thread.sleep(10);
			timeRemaining -= 10;
		}

		if (!taskFuture.isDone())
			throw new Exception("Could not stop the Producer");
	}

	private void run()
	{
		_running = true;
		while (_running)
		{

			try {
				Thread.sleep(_random.nextInt(100)+400);	// Simulate some other work being done

				Widget widget = new Widget(nextWidgetId++, _random.nextInt(100)+99);

				System.out.println("Adding "+ widget.toString() +" to the widget bag");
				widgets.put(widget);

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}